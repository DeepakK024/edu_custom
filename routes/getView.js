const model = require('../model/index');

module.exports = async (req, res) => {
    try {
        const mapperData = await model[req.body.mapper].findOne({ where: { id: req.body.id } });
        let resultObjects = {}
        if (req.body.mapper == "FormMapper") {
            resultObjects = {
                createFields: JSON.parse(JSON.parse(mapperData.createFields)),
                editFields: JSON.parse(JSON.parse(mapperData.editFields)),
                relationName: JSON.parse(JSON.parse(mapperData.relationName)),
                validator: JSON.parse(JSON.parse(mapperData.validator))
            }
        }
        else if (req.body.mapper == "FilterMapper") {
            resultObjects = {
                filterObject: JSON.parse(JSON.parse(mapperData.filterObject))
            }
        }
        else {
            resultObjects = {
                attributes: JSON.parse(JSON.parse(mapperData.attributes)),
                relationName: JSON.parse(JSON.parse(mapperData.relationName)),
                allowedAccess: JSON.parse(JSON.parse(mapperData.allowedAccess)),
                additionalFilters: JSON.parse(JSON.parse(mapperData.additionalFilters)),
                sortingOrder: JSON.parse(JSON.parse(mapperData.sortingOrder))
            }
        }
        res.status(200).send({
            message: "success",
            results: resultObjects
        })
    }
    catch (err) {
        console.log("error---- ", err)
        res.status(406).send({
            message: err.message
        })
    }
}