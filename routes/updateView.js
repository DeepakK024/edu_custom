const model = require('../model/index');

module.exports = async (req, res) => {
    try {
        let resultObjects = {}
        let mapperData = req.body;
        if (req.body.mapper == "FormMapper") {
            resultObjects = {
                createFields: JSON.stringify(JSON.stringify(mapperData.createFields)),
                editFields: JSON.stringify(JSON.stringify(mapperData.editFields)),
                relationName: JSON.stringify(JSON.stringify(mapperData.relationName)),
                validator: JSON.stringify(JSON.stringify(mapperData.validator))
            }
        }
        else if (req.body.mapper == "FilterMapper") {
            resultObjects = {
                filterObject: JSON.stringify(JSON.stringify(mapperData.filterObject)),
            }
        }
        else {
            resultObjects = {
                attributes: JSON.stringify(JSON.stringify(mapperData.attributes)),
                relationName: JSON.stringify(JSON.stringify(mapperData.relationName)),
                allowedAccess: JSON.stringify(JSON.stringify(mapperData.allowedAccess)),
                additionalFilters: JSON.stringify(JSON.stringify(mapperData.additionalFilters)),
                sortingOrder: JSON.stringify(JSON.stringify(mapperData.sortingOrder))
            }
        }
        console.log("resultObject  ", resultObjects)
        const updatedObject = await model[req.body.mapper].update(resultObjects, { where: { id: req.body.id } });
        res.status(200).send({
            message: "success",
            results: updatedObject
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.message
        })
    }
}