const model = require('../model/index');

module.exports = async (req, res) => {
    try {
        let mapperData = {}
        if (req.body.mapper == "FilterMapper") {
            mapperData = await model[req.body.mapper].findAll({ attributes: ["id", "view"] });
        }
        else {
            mapperData = await model[req.body.mapper].findAll({ where: { roleId: req.body.roleId }, attributes: ["id", "view"] });
        }

        console.log("mapperdata", mapperData)
        res.status(200).send({
            message: "success",
            results: mapperData
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.message
        })
    }
}
