'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");

const FormMapper = sequelize.define('FormMapper', {
    editFields: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('editFields')
        },
        set(value) {
            this.setDataValue('editFields', value);
        }
    },
    createFields: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('createFields')
        },
        set(value) {
            this.setDataValue('createFields', value);
        }
    },
    view: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('view')
        },
        set(value) {
            this.setDataValue('view', value);
        }
    },
    fromTable: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('fromTable')
        },
        set(value) {
            this.setDataValue('fromTable', value);
        }
    },
    relationName: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('relationName')
        },
        set(value) {
            this.setDataValue('relationName', value);
        }
    },
    validator: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('validator')
        },
        set(value) {
            this.setDataValue('validator', value);
        }
    },
    roleId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('roleId')
        },
        set(value) {
            this.setDataValue('roleId', value);
        }
    }
},
    {
        timestamps: false,
        tableName: 'formmapper'
    });
console.log("FormMapper Table", FormMapper === sequelize.models.FormMapper);
module.exports = FormMapper;