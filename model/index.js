const TableMapper = require('./tablemapper')
const FilterMapper = require('./filtermapper')
const FormMapper = require('./formmapper');
const deletemapper = require('./deletemapper');

const models = {
    TableMapper,
    FilterMapper,
    FormMapper,
    deletemapper
}
module.exports = models