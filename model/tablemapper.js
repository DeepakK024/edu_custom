'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");

const TableMapper = sequelize.define('functionmapper', {
    relationName: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('relationName')
        },
        set(value) {
            this.setDataValue('relationName', value);
        }
    },
    attributes: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('attributes')
        },
        set(value) {
            this.setDataValue('attributes', value);
        }
    },
    view: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('view')
        },
        set(value) {
            this.setDataValue('view', value);
        }
    },
    fromTable: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('fromTable')
        },
        set(value) {
            this.setDataValue('fromTable', value);
        }
    },
    allowedAccess: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('allowedAccess')
        },
        set(value) {
            this.setDataValue('allowedAccess', value);
        }
    },
    additionalFilters: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('additionalFilters')
        },
        set(value) {
            this.setDataValue('additionalFilters', value);
        }
    },
    sortingOrder: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('sortingOrder')
        },
        set(value) {
            this.setDataValue('sortingOrder', value);
        }
    },
    roleId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('roleId')
        },
        set(value) {
            this.setDataValue('roleId', value);
        }
    }
});
console.log("Mapper Table", TableMapper === sequelize.models.functionmapper);
module.exports = TableMapper;