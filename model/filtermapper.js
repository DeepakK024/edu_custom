'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");

const FilterMapper = sequelize.define('FilterMapper',{
    filterObject:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('filterObject')
        },
        set(value){
            this.setDataValue('filterObject',value);
        }
      },
      view:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('view')
        },
        set(value){
            this.setDataValue('view',value);
        }
      }
},
{
    timestamps: false,
    tableName : 'filtermapper'
});
console.log("FilterMapper Table",FilterMapper === sequelize.models.FilterMapper); 
module.exports = FilterMapper;